<?php
//
//namespace app\models;
//
//class User extends \yii\base\Object implements \yii\web\IdentityInterface
//{
//    public $id;
//    public $username;
//    public $password;
//    public $authKey;
//    public $accessToken;
//
//    private static $users = [
//        '100' => [
//            'id' => '100',
//            'username' => 'admin',
//            'password' => 'admin',
//            'authKey' => 'test100key',
//            'accessToken' => '100-token',
//        ],
//        '101' => [
//            'id' => '101',
//            'username' => 'demo',
//            'password' => 'demo',
//            'authKey' => 'test101key',
//            'accessToken' => '101-token',
//        ],
//    ];
//
//
//    /**
//     * @inheritdoc
//     */
//    public static function findIdentity($id)
//    {
//        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
//    }
//
//    /**
//     * @inheritdoc
//     */
//    public static function findIdentityByAccessToken($token, $type = null)
//    {
//        foreach (self::$users as $user) {
//            if ($user['accessToken'] === $token) {
//                return new static($user);
//            }
//        }
//
//        return null;
//    }
//
//    /**
//     * Finds user by username
//     *
//     * @param string $username
//     * @return static|null
//     */
//    public static function findByUsername($username)
//    {
//        foreach (self::$users as $user) {
//            if (strcasecmp($user['username'], $username) === 0) {
//                return new static($user);
//            }
//        }
//
//        return null;
//    }
//
//    /**
//     * @inheritdoc
//     */
//    public function getId()
//    {
//        return $this->id;
//    }
//
//    /**
//     * @inheritdoc
//     */
//    public function getAuthKey()
//    {
//        return $this->authKey;
//    }
//
//    /**
//     * @inheritdoc
//     */
//    public function validateAuthKey($authKey)
//    {
//        return $this->authKey === $authKey;
//    }
//
//    /**
//     * Validates password
//     *
//     * @param string $password password to validate
//     * @return bool if password provided is valid for current user
//     */
//    public function validatePassword($password)
//    {
//        return $this->password === $password;
//    }
//}
namespace app\models;
use Yii;
 
use yii\db\ActiveRecord;
 
class User extends ActiveRecord implements \yii\web\IdentityInterface
{
 
public static function tableName() { return 'user'; }
 
   /**
 * @inheritdoc
 */
  public function rules()
  {
        return [
            [['username','password'], 'required'],
            [['user_level'], 'string'],
            [['email'], 'email'],
            [['username','email'], 'unique'],
 			[['phone_number'],'string','max' => 30],
		    [['username','password','password_reset_token','first_name','last_name'], 'string', 'max' => 250],
            [['user_image','email'], 'string', 'max' => 500],
        ];
  }
 
public static function findIdentity($id) {
    $user = self::find()
            ->where([
                "id" => $id
            ])
            ->one();
    if (!count($user)) {
        return null;
    }
    return new static($user);
}
 
/**
 * @inheritdoc
 */
public static function findIdentityByAccessToken($token, $userType = null) {
 
    $user = self::find()
            ->where(["accessToken" => $token])
            ->one();
    if (!count($user)) {
        return null;
    }
    return new static($user);
}
 
/**
 * Finds user by username
 *
 * @param  string      $username
 * @return static|null
 */
public static function findByUsername($username) {
    $user = self::find()
            ->where([
                "username" => $username
            ])
            ->one();
    if (!count($user)) {
        return null;
    }
    return new static($user);
}
 
public static function findByUser($username) {
    $user = self::find()
            ->where([
                "username" => $username
            ])
            ->one();
    if (!count($user)) {
        return null;
    }
    return $user;
}
 
/**
 * @inheritdoc
 */
public function getId() {
    return $this->id;
}
 
/**
 * @inheritdoc
 */
public function getAuthKey() {
    return $this->authKey;
}
 
/**
 * @inheritdoc
 */
public function validateAuthKey($authKey) {
    return $this->authKey === $authKey;
}
 
/**
 * Validates password
 *
 * @param  string  $password password to validate
 * @return boolean if password provided is valid for current user
 */
public function validatePassword($password) {
//	if (Yii::$app->getSecurity()->validatePassword($password, $hash)) {
//	    // all good, logging user in
//	} else {
//	    // wrong password
//	}
//    return $this->password ===  md5($password);
return Yii::$app->getSecurity()->validatePassword($password, $this->password);

}
 
}
