//<?php
//
//namespace app\models;
//
//use Yii;
//
///**
// * This is the model class for table "user".
// *
// * @property integer $id
// * @property string $first_name
// * @property string $last_name
// * @property string $phone_number
// * @property string $username
// * @property string $email
// * @property string $password
// * @property string $authKey
// * @property string $password_reset_token
// * @property string $user_image
// * @property string $user_level
// */
//class UserSearch extends \yii\db\ActiveRecord
//{
//    /**
//     * @inheritdoc
//     */
//    public static function tableName()
//    {
//        return 'user';
//    }
//
//    /**
//     * @inheritdoc
//     */
//    public function rules()
//    {
//        return [
//            [['first_name', 'last_name', 'phone_number', 'username', 'email', 'password', 'authKey', 'password_reset_token', 'user_image'], 'required'],
//            [['user_level'], 'string'],
//            [['first_name', 'last_name', 'username', 'password', 'authKey', 'password_reset_token'], 'string', 'max' => 250],
//            [['phone_number'], 'string', 'max' => 30],
//            [['email', 'user_image'], 'string', 'max' => 500],
//            [['username'], 'unique'],
//        ];
//    }
//
//    /**
//     * @inheritdoc
//     */
//    public function attributeLabels()
//    {
//        return [
//            'id' => 'ID',
//            'first_name' => 'First Name',
//            'last_name' => 'Last Name',
//            'phone_number' => 'Phone Number',
//            'username' => 'Username',
//            'email' => 'Email',
//            'password' => 'Password',
//            'authKey' => 'Auth Key',
//            'password_reset_token' => 'Password Reset Token',
//            'user_image' => 'User Image',
//            'user_level' => 'User Level',
//        ];
//    }
//}
namespace app\models;
 
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;
 
/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['first_name', 'last_name', 'phone_number', 'username', 'email', 'password', 'authKey', 'password_reset_token', 'user_image', 'user_level'], 'safe'],
        ];
    }
 
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
 
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();
 
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
 
        $this->load($params);
 
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
 
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
 
        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'authKey', $this->authKey])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'user_image', $this->user_image])
            ->andFilterWhere(['like', 'user_level', $this->user_level]);
 
        return $dataProvider;
    }
}