<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status`.
 */
class m170712_053023_create_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status', [
            	'id' => $this->primaryKey(),
              	'message' => $this->text()->notNull()->defaultValue(''),
              	'permissions' => $this->integer()->notNull()->defaultValue(0),
              	'created_at' => $this->integer()->notNull(),
              	'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('status');
    }
}
