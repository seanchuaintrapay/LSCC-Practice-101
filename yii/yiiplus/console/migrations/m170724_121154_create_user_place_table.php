<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_place`.
 */
class m170724_121154_create_user_place_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_place', [
            'id' => $this->primaryKey(),
            'user_id' => $this->bigInteger()->notNull(),
            'place_id' => $this->integer()->notNull(),
            'is_favorite' => $this->smallInteger()->notNull()->defaultValue(0),
            'number_meetings' => $this->integer()->defaultValue(0),
            'is_special' => $this->smallInteger()->notNull()->defaultValue(0),
            'note' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk_user_place_user', '{{%user_place}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');     
        $this->addForeignKey('fk_user_place_place', '{{%user_place}}', 'place_id', '{{%place}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_place');
    }
}
