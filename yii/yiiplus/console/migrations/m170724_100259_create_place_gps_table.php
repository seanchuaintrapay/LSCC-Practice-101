<?php

use yii\db\Migration;

/**
 * Handles the creation of table `place_gps`.
 */
class m170724_100259_create_place_gps_table extends Migration
{
    /**
     * @inheritdoc
     */
     
    public function up()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=MyISAM';
      }
      
        $this->createTable('place_gps', [
            'id' => $this->primaryKey(),
            'place_id' => $this->integer()->notNull(),
            'gps' => 'POINT NOT NULL'
        ],$tableOptions);
        $this->execute('create spatial index place_gps_gps on '.'{{%place_gps}}(gps);');
        $this->addForeignKey('fk_place_gps','{{%place_gps}}' , 'place_id', '{{%place}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('place_gps');
    }
}
