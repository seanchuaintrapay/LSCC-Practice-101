<?php

use yii\db\Migration;

/**
 * Handles the creation of table `meeting`.
 */
class m170724_085749_create_meeting_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
    	$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        
        $this->createTable('meeting', [
            'id' => $this->primaryKey(),
            'owner_id' => $this->bigInteger()->notNull(),
            'meeting_type' => $this->smallInteger()->notNull()->defaultValue(0),
            'message' => $this->text()->notNull()->defaultValue(''),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'update_at' => $this->integer()->notNull(),
            
        ], $tableOptions);
        $this->addForeignKey('fk_meeting_owner', '{{%meeting}}', 'owner_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_meeting_owner', '{{%meeting}}');
        $this->dropTable('{{%meeting}}');
    }
}
