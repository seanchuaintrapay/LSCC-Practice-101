<?php

use yii\db\Migration;

/**
 * Handles the creation of table `meeting_time`.
 */
class m170724_104656_create_meeting_time_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('meeting_time', [
            'id' => $this->primaryKey(),
            'meeting_id' => $this->integer()->notNull(),
            'start' => $this->integer()->notNull(),
            'suggested_by' => $this->bigInteger()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk_meeting_time_meeting', '{{%meeting_time}}', 'meeting_id', '{{%meeting}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_participant_suggested_by', '{{%meeting_time}}', 'suggested_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('meeting_time');
    }
}
