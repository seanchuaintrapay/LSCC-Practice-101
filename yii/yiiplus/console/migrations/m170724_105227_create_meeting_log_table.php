<?php

use yii\db\Migration;

/**
 * Handles the creation of table `meeting_log`.
 */
class m170724_105227_create_meeting_log_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('meeting_log', [
            'id' => $this->primaryKey(),
            'meeting_id' => $this->integer()->notNull(),
            'action' => $this->integer()->notNull(),
            'actor_id' => $this->bigInteger()->notNull(),
            'item_id' => $this->integer()->notNull(),
            'extra_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk_meeting_log_meeting', '{{%meeting_log}}', 'meeting_id', '{{%meeting}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_meeting_log_actor', '{{%meeting_log}}', 'actor_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('meeting_log');
    }
}
