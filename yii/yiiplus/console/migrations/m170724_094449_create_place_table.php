<?php

use yii\db\Migration;

/**
 * Handles the creation of table `place`.
 */
class m170724_094449_create_place_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('place', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'place_type' =>$this->smallInteger()->notNull()->defaultValue(0),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'google_place_id' =>$this->string()->notNull(),
            'created_by' => $this->bigInteger()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'update_at' => $this->integer()->notNull(),
        ],'ENGINE=InnoDB');
        $this->addForeignKey('fk_place_created_by', '{{%place}}', 'created_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }
    

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('place');
    }
}
