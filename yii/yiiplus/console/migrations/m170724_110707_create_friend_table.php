<?php

use yii\db\Migration;

/**
 * Handles the creation of table `friend`.
 */
class m170724_110707_create_friend_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('friend', [
            'id' => $this->primaryKey(),
            'user_id' => $this->bigInteger()->notNull(),
            'friend_id' => $this->bigInteger()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'number_meetings' => $this->integer()->notNull()->defaultValue(0),
            'is_favorite' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk_friend_user_id', '{{%friend}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');     
        $this->addForeignKey('fk_friend_friend_id', '{{%friend}}', 'friend_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('friend');
    }
}
