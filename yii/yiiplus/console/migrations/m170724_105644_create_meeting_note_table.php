<?php

use yii\db\Migration;

/**
 * Handles the creation of table `meeting_note`.
 */
class m170724_105644_create_meeting_note_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('meeting_note', [
            'id' => $this->primaryKey(),
            'meeting_id' => $this->integer()->notNull(),
            'posted_by' => $this->bigInteger()->notNull()->defaultValue(0),
            'note' => $this->text(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk_meeting_note_meeting', '{{%meeting_note}}', 'meeting_id', '{{%meeting}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_meeting_note_posted_by', '{{%meeting_note}}', 'posted_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('meeting_note');
    }
}
