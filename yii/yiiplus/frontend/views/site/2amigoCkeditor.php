<?php
use dosamigos\ckeditor\CKEditorInline;

// First we need to tell CKEDITOR variable where is our external plugin 
$this->registerJs("CKEDITOR.plugins.addExternal('pbckcode', '/pbckcode/plugin.js', '');");

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">

// Using the plugin
<?php CKEditorInline::begin(['preset' => 'custom', 'clientOptions' => [
    'extraPlugins' => 'pbckcode',
    'toolbarGroups' => [
        ['name' => 'undo'],
        ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
        ['name' => 'colors'],
        ['name' => 'links', 'groups' => ['links', 'insert']],
        ['name' => 'others', 'groups' => ['others', 'about']],
        
        ['name' => 'pbckcode'] // <--- OUR NEW PLUGIN YAY!
    ]
]]) ?>

<p>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
    dolore magna aliqua. 
</p>
<?php CKEditorInline::end() ?>

    </div>
</div>
