<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%place}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $place_type
 * @property integer $status
 * @property string $google_place_id
 * @property string $created_by
 * @property integer $created_at
 * @property integer $update_at
 *
 * @property MeetingPlace[] $meetingPlaces
 * @property User $createdBy
 * @property UserPlace[] $userPlaces
 */
class Place extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%place}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'google_place_id', 'created_by', 'created_at', 'update_at'], 'required'],
            [['place_type', 'status', 'created_by', 'created_at', 'update_at'], 'integer'],
            [['name', 'google_place_id'], 'string', 'max' => 255],
            //[['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'place_type' => 'Place Type',
            'status' => 'Status',
            'google_place_id' => 'Google Place ID',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'update_at' => 'Update At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetingPlaces()
    {
        return $this->hasMany(MeetingPlace::className(), ['place_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPlaces()
    {
        return $this->hasMany(UserPlace::className(), ['place_id' => 'id']);
    }
    
    public function getPlaceType($data)
    {
    	$option = $this->getPlaceTypeOptions();
    	return $options[$data];
    }
    
    public function getPlaceTypeOptions()
    {
    	return array(
    		self::TYPE_RETAURANT => 'Restaurant',
    		self::TYPE_CONFFEE => 'Coffeeshop',
    		self::TYPE_RESIDENCE => 'Residence',
    		self::TYPE_OFFICE => 'Office',
    		self::TYPE_BAR => 'Bar',
    		self::TYPE_BAR => 'Other'
    	);
    }
    
    public function addGeometryByPoint($model,$lat,$lon) {
        $pg = new PlaceGPS;
        $pg->place_id=$model->id;
        $pg->gps = new \yii\db\Expression("GeomFromText('Point(".$lat." ".$lon.")')");
        $pg->save();    
    }
}
