<?php

use yii\db\Migration;

/**
 * Handles adding status to table `about`.
 */
class m170928_092730_add_status_column_to_about_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('about', 'status', $this->integer()->notNull()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('about', 'status');
    }
}
