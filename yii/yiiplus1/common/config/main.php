<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
    	'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=yiiplus1',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
            'urlManager' => [
	        'class' => 'yii\web\UrlManager',
	        // Hide index.php
	        'showScriptName' => false,
	        // Use pretty URLs
	        'enablePrettyUrl' => true,
	        'rules' => [
	        ],
	    ],
    ],
];
