<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "About".
 *
 * @property integer $id
 * @property string $body
 * @property integer $created_at
 * @property integer $updated_at
 */
class About extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'About';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body', 'created_at', 'updated_at'], 'required'],
            [['body'], 'string'],
            [['status'], 'integer'],
            [['created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'body' => 'Body',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
