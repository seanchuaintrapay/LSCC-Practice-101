<?php

use yii\db\Migration;

/**
 * Handles adding created_by to table `docs`.
 */
class m170811_085141_add_created_by_column_to_docs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('docs', 'created_by', $this->integer()->notNull()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('docs', 'created_by');
    }
}
