<?php

use yii\db\Migration;

/**
 * Handles the creation of table `docs`.
 */
class m170810_083940_create_docs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('docs', [
            'id' => $this->bigPrimaryKey(),
            'title' => $this->string()->notNull(),
            'body' => $this->text()->notNull()->defaultValue(''),
            'order_by' => $this->string()->notNull(),
            'created_by' => $this->integer()->notNull()->defaultValue(99),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ],$tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('docs');
    }
}
