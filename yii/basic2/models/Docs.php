<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "docs".
 *
 * @property string $id
 * @property string $title
 * @property string $body
 * @property integer $order_by
 * @property integer $created_at
 * @property integer $updated_at
 */
class Docs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'docs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'body', 'created_at', 'updated_at'], 'required'],
            [['body'], 'string'],
            [['order_by', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'order_by' => 'Order By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
